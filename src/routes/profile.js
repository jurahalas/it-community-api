import express from 'express';
import { authJwt }  from '../services/passport';
import User from '../models/User';
import Profile from '../models/Profile';
import {
  profileValidate,
  validateExperienceInput,
  validateEducationInput,
  getProfileByHandleValidate,
  getProfileByUseIdValidate
} from "../services/validation";
import { parser } from '../services/cloudinary';

const router = express.Router();

router.get('/', authJwt, (req, res) => {
    Profile.findOne({ user: req.user.id })
      .populate('user', ['name', 'avatar'])
      .then(profile => {
        if (!profile) {

          User.findById(req.user.id)
            .then(user => {
              return res.json({
                noprofile : 'There is no profile for this user',
                status: 'NonExist',
                user: user.name
              });
            })
            .catch(err => res.json(err))

        } else {
          res.json(profile);
        }

      })
      .catch(err => res.status(404).json(err));
  }
);

router.post('/', authJwt, (req, res) => {
  const { errors, isValid } = profileValidate(req.body);

  if(!isValid) {
    return res.status(400).json(errors);
  }
    const {
      name,
      handle,
      company,
      website,
      location,
      bio,
      status,
      githubUsername,
      bitbucketUsername,
      skills,
      google,
      twitter,
      facebook,
      linkedin
    } = req.body;

    const profileFields = {
      user: req.user.id,
      handle: handle,
      company: company,
      website: website,
      location: location,
      bio: bio,
      status: status,
      githubUsername: githubUsername,
      bitbucketUsername: bitbucketUsername,
      skills: skills,
      social: {
        google: google,
        twitter: twitter,
        facebook: facebook,
        linkedin: linkedin,
      }
    };

    Profile.findOne({ user: req.user.id }).then(profile => {
      if(profile) {
        User.updateOne(
          { _id: req.user.id },
          {
            "$set": {
              "name": name,
            },
          },
          { new: true }

        ).then(() => {
          Profile.findOneAndUpdate(
            { user: req.user.id },
            { $set: profileFields },
            { new: true }
          )
            .populate('user', ['name', 'avatar'])
            .then(profile => res.json(profile));

        })

      } else {

        Profile.findOne({ handle: profileFields.handle }).then(profile => {
          if(profile) {
            errors.handle = 'That handle already exists';
            res.status(400).json(errors);
          }

          new Profile(profileFields).save().then(profile => res.json(profile));
        });
      }
    });
  });


router.get('/all', (req, res) => {
  const errors = {};

  Profile.find()
    .populate('user', ['name', 'avatar'])
    .then(profiles => {
      if (!profiles) {
        errors.noprofile = 'There are no profiles';
        return res.status(404).json(errors);
      }

      res.json(profiles);
    })
    .catch(err => res.status(404).json({ profile: 'There are no profiles' }));
});

router.get('/handle/:handle', (req, res) => {

  const { errors, isValid } = getProfileByHandleValidate(req.params);

  if(!isValid) {
    return res.status(400).json(errors);
  }

  Profile.findOne({ handle: req.params.handle })
    .populate('user', ['name', 'avatar'])
    .then(profile => {
      if (!profile) {
        errors.noprofile = 'There is no profile for this user';
        res.status(404).json(errors);
      }

      res.json(profile);
    })
    .catch(err => res.status(404).json(err));
});

router.get('/user/:user_id', (req, res) => {

  const { errors, isValid } = getProfileByUseIdValidate(req.params);

  if(!isValid) {
    return res.status(400).json(errors);
  }

  Profile.findOne({ user: req.params.user_id })
    .populate('user', ['name', 'avatar'])
    .then(profile => {
      if (!profile) {
        errors.noprofile = 'There is no profile for this user';
        res.status(404).json(errors);
      }

      res.json(profile);
    })
    .catch(err =>
      res.status(404).json({ profile: 'There is no profile for this user' })
    );
});

router.post('/experience', authJwt, (req, res) => {
    const { errors, isValid } = validateExperienceInput(req.body);

    if (!isValid) {
      return res.status(400).json(errors);
    }

    Profile.findOne({ user: req.user.id }).then(profile => {
      const newExp = {
        title: req.body.title,
        company: req.body.company,
        location: req.body.location,
        from: req.body.from,
        to: req.body.to,
        current: req.body.current,
        description: req.body.description
      };

      profile.experience.unshift(newExp);

      profile.save().then(profile => res.json(profile));
    });
  }
);

router.get('/experience/:exp_id', authJwt, (req, res) => {

    Profile.findOne({ user: req.user.id })
      .then(profile => {
      const experienceItem = profile.experience.filter(item => item.id === req.params.exp_id);
      res.json(experienceItem)
    })
      .catch(err => res.status(404).json(err));
  }
);


router.post('/experience/:exp_id', authJwt, (req, res) => {
    const { errors, isValid } = validateExperienceInput(req.body);

    if (!isValid) {
      return res.status(400).json(errors);
    }

    Profile.updateOne(
      {
        user : req.user.id,
        "experience._id": req.params.exp_id
      },
      {
        "$set": {
          "experience.$.title": req.body.title,
          "experience.$.company": req.body.company,
          "experience.$.location": req.body.location,
          "experience.$.from": req.body.from,
          "experience.$.to": req.body.to,
          "experience.$.current": req.body.current,
          "experience.$.description": req.body.description,
        }
      })
      .then(() =>{
        res.json({success: true})
      })
      .catch(err => res.status(404).json(err));
  }
);

router.delete('/experience/:exp_id', authJwt, (req, res) => {

    Profile.findOne({ user: req.user.id })
      .populate('user', ['name', 'avatar'])
      .then(profile => {
        const removeIndex = profile.experience
          .map(item => item.id)
          .indexOf(req.params.exp_id);

        profile.experience.splice(removeIndex, 1);

        profile.save().then(profile => res.json(profile));
      })
      .catch(err => res.status(404).json(err));
  }
);

router.post('/education', authJwt, (req, res) => {
    const { errors, isValid } = validateEducationInput(req.body);

    if (!isValid) {
      return res.status(400).json(errors);
    }

    Profile.findOne({ user: req.user.id }).then(profile => {
      const newEdu = {
        school: req.body.school,
        degree: req.body.degree,
        fieldOfStudy: req.body.fieldOfStudy,
        from: req.body.from,
        to: req.body.to,
        current: req.body.current,
        description: req.body.description
      };

      profile.education.unshift(newEdu);

      profile.save().then(profile => res.json(profile));
    });
  }
);

router.get('/education/:edu_id', authJwt, (req, res) => {

    Profile.findOne({ user: req.user.id })
      .then(profile => {
        const educationItem = profile.education.filter(item => item.id === req.params.edu_id);
        res.json(educationItem)
      })
      .catch(err => res.status(404).json(err));
  }
);

router.post('/education/:edu_id', authJwt, (req, res) => {
    const { errors, isValid } = validateEducationInput(req.body);

    if (!isValid) {
      return res.status(400).json(errors);
    }

    Profile.updateOne(
      {
        user : req.user.id,
        "education._id": req.params.edu_id
      },
      {
        "$set": {
          "education.$.school": req.body.school,
          "education.$.degree": req.body.degree,
          "education.$.fieldOfStudy": req.body.fieldOfStudy,
          "education.$.from": req.body.from,
          "education.$.to": req.body.to,
          "education.$.current": req.body.current,
          "education.$.description": req.body.description,
        }
      })
      .then(() =>{
        res.json({success: true})
      })
      .catch(err => res.status(404).json(err));
  }
);

router.delete('/education/:edu_id', authJwt, (req, res) => {
    Profile.findOne({ user: req.user.id })
      .populate('user', ['name', 'avatar'])
      .then(profile => {
        const removeIndex = profile.education
          .map(item => item.id)
          .indexOf(req.params.edu_id);

        profile.education.splice(removeIndex, 1);

        profile.save().then(profile => res.json(profile));
      })
      .catch(err => res.status(404).json(err));
  }
);

router.delete( '/', authJwt, (req, res) => {
    Profile.findOneAndRemove({ user: req.user.id }).then(() => {
      User.findOneAndRemove({ _id: req.user.id }).then(() =>
        res.json({ success: true })
      );
    });
  }
);

router.post('/images', parser.single("file"), authJwt, (req, res) => {
  const avatar = {};
  avatar.url = req.file.url;
  avatar.id = req.file.public_id;
  User.findOneAndUpdate(
    { _id: req.user.id },
    {
      "$set": {
        "avatar": req.file.url,
      },
    },
    { new: true }
  ).then(user=>{
    res.json({avatar: user.avatar})
  })
    .catch(err => res.status(404).json(err));
});

export default router;