import express from 'express';
import { authJwt }  from '../services/passport';
import User from '../models/User';
import Profile from '../models/Profile';
import Post from '../models/Post';
import { validatePostInput  } from "../services/validation";

const router = express.Router();

router.get('/', authJwt, (req, res) => {
  Post.find()
    .sort({ date: -1 })
    .then(posts => res.json(posts))
    .catch(err => res.status(404).json({ noPostsFound: 'No posts found' }))
});

router.get('/byCurrentUser', authJwt, (req, res)=>{
  Post.findOne({ user: req.user.id })
    .sort({ date: -1 })
    .then(posts => res.json(posts))
    .catch(err => res.status(404).json({ noPostsFound: 'No posts found' }))
});

router.get('/:id', (req,res) => {
  Post.findById(req.params.id)
    .then(post => res.json(post))
    .catch(err => res.status(404).json({ noPostFound: 'No post found with that id' }))
});

router.post('/', authJwt, (req,res)=>{
  const { errors, isValid } = validatePostInput(req.body);

  if(!isValid) {
    return res.status(400).json(errors);
  }

  const newPost = new Post({
    text: req.body.text,
    name: req.body.name,
    avatar: req.body.avatar,
    user: req.user.id,
  });

  newPost.save()
    .then(()=>{
        Post.find()
          .sort({ date: -1 })
          .then(posts =>{
            req.io.emit('getAllPosts', posts);
            return res.json(posts)
          } );
    }).catch(err => res.status(400).json(err));

});

router.delete('/:id', authJwt, (req,res) => {
  Profile.findOne({ user: req.user.id })
    .then(() =>{
      Post.findById(req.params.id)
        .then(post => {
          if(post.user.toString() !== req.user.id) {
            return res.status(401).json({ notAuthorized: 'User not authorized' })
          }

          post.remove().then(()=>{
            Post.find()
              .sort({ date: -1 })
              .then(posts =>{
                req.io.emit('getAllPosts', posts);
                return res.json(posts)
              } );
          }).catch(err => res.status(400).json(err));
        })
        .catch(err => res.status(404).json({ postNotFound: 'No post found' }))
    })
});

router.post('/like/:id', authJwt, (req, res) =>{
  Profile.findOne({ user: req.user.id }).then(() => {
    Post.findById(req.params.id)
      .then(post => {
        if(post.likes.filter(like => like.user.toString() === req.user.id).length > 0) {
          const removeIndex = post.likes.map(item => item.user.toString()).indexOf(req.user.id);

          post.likes.splice(removeIndex, 1);
        } else {
          if(post.disLikes.filter(like => like.user.toString() === req.user.id).length > 0) {
            const removeIndex = post.disLikes.map(item => item.user.toString()).indexOf(req.user.id);

            post.disLikes.splice(removeIndex, 1);
          }
          post.likes.unshift({ user: req.user.id });
        }

        post.save().then(()=>{
          Post.find()
            .sort({ date: -1 })
            .then(posts =>{
              req.io.emit('getAllPosts', posts);
              return res.json(posts)
            } );
        }).catch(err => res.status(400).json(err));
      })
      .catch(err => res.status(404).json(err))
  })
});

router.post('/unlike/:id', authJwt, (req,res) =>{
  Profile.findOne({ user: req.user.id }).then(() => {
    Post.findById(req.params.id)
      .then(post => {
        if(post.disLikes.filter(like => like.user.toString() === req.user.id).length > 0) {
          const removeIndex = post.disLikes.map(item => item.user.toString()).indexOf(req.user.id);

          post.disLikes.splice(removeIndex, 1);
        } else {
          if(post.likes.filter(like => like.user.toString() === req.user.id).length > 0) {
            const removeIndex = post.likes.map(item => item.user.toString()).indexOf(req.user.id);

            post.likes.splice(removeIndex, 1);
          }
          post.disLikes.unshift({ user: req.user.id });
        }

        post.save().then(()=>{
          Post.find()
            .sort({ date: -1 })
            .then(posts =>{
              req.io.emit('getAllPosts', posts);
              return res.json(posts)
            } );
        }).catch(err => res.status(400).json(err));
      })
      .catch(err => res.status(404).json)
  })
});

router.post('/comment/:id', authJwt, (req,res) =>{
  const { errors, isValid } = validatePostInput(req.body);

  if(!isValid) {
    return res.status(400).json(errors);
  }

  Post.findById(req.params.id)
    .then(post => {
      const newComment = {
        text: req.body.text,
        name: req.body.name,
        avatar: req.body.avatar,
        user: req.user.id,
      };

      post.comments.unshift(newComment);

      post.save()
        .then(post =>{
          req.io.emit('getComment', post);
          return res.json(post)
        });
    })
    .catch(err => res.status(404).json.json({ postNoFind: 'Post no find' }))
});

router.delete('/comment/:id/:comment_id', authJwt, (req,res) =>{
  Post.findById(req.params.id)
    .then(post => {
      if(post.comments.filter(comment => comment._id.toString() === req.params.comment_id).length === 0) {
        return res.status(404).json({ commentNotExist: 'Comment does not exist' })
      }

      const removeIndex = post.comments.map(item => item._id.toString()).indexOf(req.params.comment_id);
      post.comments.splice(removeIndex, 1);

      post.save().then(post =>{
        req.io.emit('getComment', post);
        return res.json(post)
      });
    })
    .catch(err => res.status(404).json.json({ postNoFind: 'Post no find' }))
});

export default router;