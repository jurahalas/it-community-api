import express from 'express';
import { authJwt, authFacebook, authFacebookCallback, authGoogle, authGoogleCallback }  from '../services/passport';
import User from '../models/User';
import { registerValidate, loginValidate } from '../services/validation';

const router = express.Router();

router.post('/login', (req,res)=>{
  const { email, password } = req.body;
  const { errors, isValid } = loginValidate(req.body);

  if(!isValid) {
    return res.status(400).json(errors);
  }

  User.findOne({ email: email }).then(user => {
    if(user && user.isValidPassword(password)){

      res.json({ user:user.toAuthJSON() })
    } else {
      res.status(401).json({ errors: 'Email or Password is incorrect' });
    }
  });
});

router.post('/register', (req, res) => {
  const { email, password, name } = req.body;
  const { errors, isValid } = registerValidate(req.body);

  if(!isValid) {
    return res.status(400).json(errors);
  }

  const newUser = new User({
    name,
    email,
  });

  newUser.setPassword(password);

  newUser
    .save()
    .then(userRecord => {
      res.json({ user: userRecord.toAuthJSON() })})
    .catch(err => res.status(400).json({ errors: err.errors }));
});

router.get('/current', authJwt, (req, res) =>{
    res.json({
      id: req.user.id,
      name: req.user.name,
      email: req.user.email
    })
  });

router.get('/auth/facebook', authFacebook);

router.get('/auth/facebook/callback', authFacebookCallback, (req, res)=> {
  const user = req.user.toAuthJSON();
  res.redirect("http://localhost:3000/#/success?token=" + user.token);
});

router.get('/auth/google', authGoogle);

router.get('/auth/google/callback', authGoogleCallback, (req, res)=> {
  const user = req.user.toAuthJSON();
  res.redirect("http://localhost:3000/#/success?token=" + user.token);
});

export default router;
