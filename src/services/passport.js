import passport from "passport";
import { Strategy as JWTStrategy, ExtractJwt } from "passport-jwt";
import { Strategy as FacebookStrategy } from "passport-facebook";
import { Strategy as GoogleStrategy } from "passport-google-oauth20";
import User from '../models/User';
import dotenv from 'dotenv';
import * as cloudinary from 'cloudinary';

dotenv.config();


const jwtOpts  = {
  secretOrKey: process.env.JWT_SECRET,
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken()
};

const jwtLogin = new JWTStrategy(jwtOpts, async (payload, done) => {
  try {
    const user = await User.findById(payload.id);
    if (!user) {
      return done(null, false);
    }
    return done(null, user);
  } catch (error) {
    return done(error, false);
  }
});

const facebookOpts = {
  clientID: process.env.FACEBOOK_APP_ID,
  clientSecret: process.env.FACEBOOK_APP_SECRET,
  callbackURL: "/users/auth/facebook/callback",
  profileFields: ["id", "emails", 'picture.type(large)', "displayName"],
  passReqToCallback: true,
  proxy: true,
  enableProof: true
};

const facebookLogin = new FacebookStrategy(
  facebookOpts,
  async (req, accessToken, refreshToken, profile, done) => {
    try {

      if (!req.user) {
        let user = await User.findOne({
          facebookId: profile.id
        });

        /** User exist */
        if (user) {
          return done(null, user);
        }

        /** User not exist */
        /** Create a user using the profile come back from facebook */
        const { id, email, name } = profile._json;

        user = new User({
          name: name,
          email: email,
          facebookId: id,
          avatar: profile.photos && profile.photos[0].value,
         // avatar: `https://res.cloudinary.com/jurahalas/image/facebook/${id}.jpg`
        });

        await user.save();
        done(null, user);
      } else {
        const user = await User.findOne({ _id: req.user.id });
        user.facebookId = profile.id;
        user.save();
        done(null, user);
      }
    } catch (error) {
      done(error, false);
    }
  }
);

const googleOpts = {
  clientID: process.env.GOOGLE_CLIENT_ID,
  clientSecret: process.env.GOOGLE_CLIENT_SECRET,
  callbackURL: "/users/auth/google/callback",
  passReqToCallback: true,
  proxy: true
};

const googleLogin = new GoogleStrategy(
  googleOpts,
  async (req, accessToken, refreshToken, profile, done) => {
    try {
      if (!req.user) {
        let user = await User.findOne({ googleId: profile.id });
        /** User exist */
        if (user) {
          user.provider = 'gmail';
          return done(null, user);
        }

        let gmailUser = await User.findOne({ email: profile.emails[0].value });
        const { id, emails, displayName, image } = profile._json;

        if(!gmailUser) {
          /** User not exist */
          /** Create a user using the profile come back from google auth */

          user = new User({
            name: displayName,
            googleDisplayName: displayName,
            email: emails[0].value,
            googleId: id,
            googleAvatar: image.url,
            provider: 'gmail'
          });
          await user.save();
          done(null, user);
        } else {
          gmailUser.googleId = profile.id;
          gmailUser.provider = 'gmail';
          gmailUser.googleDisplayName = displayName;
          gmailUser.googleAvatar = image.url;

          await gmailUser.save();
          done(null, gmailUser);
        }

      } else {
        const user = await User.findOne({ _id: req.user.id });
        user.googleId = profile.id;
       user.provider = 'gmail';
        await user.save();
        done(null, user);
      }
    } catch (error) {
      done(error, false);
    }
  }
);

passport.use(jwtLogin);
passport.use(facebookLogin);
passport.use(googleLogin);

export const authJwt = passport.authenticate("jwt", { session: false });
export const authFacebook = passport.authenticate("facebook", {
  scope: "email"
});

export const authFacebookCallback = passport.authenticate("facebook", { session: false, failureRedirect: '/login' } );

export const authGoogle = passport.authenticate("google", {
  scope: [
    "email"
  ]
});
export const authGoogleCallback = passport.authenticate("google", { session: false, failureRedirect: '/login' });
