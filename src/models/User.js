import mongoose from 'mongoose';
import uniqueValidator from "mongoose-unique-validator";
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

// Create Schema
const UserSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
    lowercase: true,
    index: true,
    unique: true
  },
  password: {
    type: String,
  },
  avatar: {
    type: String
  },
  data: {
    type: Date,
    default: Date.now()
  },
  facebookId: String,
  facebookDisplayName: String,
  googleId: String,
  googleDisplayName: String,
  googleAvatar: String,
  provider: String
});

UserSchema.methods.isValidPassword = function isValidPassword(password) {
  return bcrypt.compareSync(password, this.password);
};

UserSchema.methods.setPassword = function setPassword(password) {
  this.password = bcrypt.hashSync(password, 10);
};

UserSchema.methods.generateJWT = function generateJWT() {
  return jwt.sign({
      id: this.id,
      name: this.name,
      email: this.email,
      avatar: this.avatar,
      googleAvatar: this.googleAvatar || null
    },
    process.env.JWT_SECRET,
    { expiresIn: 36000 },
  );
};

UserSchema.methods.toAuthJSON = function toAuthJSON() {
  return {
    email: this.email,
    name: this.name,
    avatar: this.avatar,
    googleAvatar: this.googleAvatar,
    token: this.generateJWT()
  }
};

UserSchema.plugin(uniqueValidator, { message: 'This email is already taken' });

export default mongoose.model('User', UserSchema);