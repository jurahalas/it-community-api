import express from 'express';
import path from 'path';
import mongoose from 'mongoose';
import bodyParser from 'body-parser';
import dotenv from 'dotenv';
import users from './routes/users';
import profile from './routes/profile';
import posts from './routes/posts';
import cors from 'cors';
import socket from 'socket.io';

const passport = require('passport');

dotenv.config();
const port = process.env.PORT || 5001;
const app = express();

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

mongoose
  .connect(encodeURI(process.env.MONGODB_URL), { useNewUrlParser: true } )
  .then(()=> console.log('MongoDB Connected'))
  .catch(err=> console.log(err));
mongoose.set('useCreateIndex', true);

app.use(passport.initialize());
app.use(passport.session());

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, 'index.html'));
});

const server = app.listen(port, () => console.log(`Running on localhost: ${port}`));
const io = socket(server);

io.on('connection', (socket) => {
  console.log("connected");
  console.log(socket.id);
});

app.use((req, res, next)=> {
  req.io = io;
  next();
});

app.use('/users', users);
app.use('/profile', profile);
app.use('/posts', posts);